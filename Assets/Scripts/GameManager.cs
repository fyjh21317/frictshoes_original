﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using System.IO;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    // TODO: calibration section
    // TODO: training section
    // TODO: testing section

    // user ID and foot
    public string userID;
    public string whichFirst;
    public float modifyheight;

    // a reference to the action
    public SteamVR_Action_Boolean userEndtrial;
    public SteamVR_Action_Boolean userEndtask;
    public SteamVR_Action_Boolean userAdjustup;
    public SteamVR_Action_Boolean userAdjustdown;

    // a reference to the hand
    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Vibration hapticAction;

    // current feet (L/R)
    public char LR;
    public int curRound;
    private const int trialsPerFoot = 3;
    private const int numOfLevels = 5;

    private GameObject headset;
    public SerialManager leftShoe;
    public SerialManager rightShoe;
    private List<int> order = new List<int>();
    //private List<float> dataCollected = new List<float>();
    private float[] dataCollected = new float[2 * trialsPerFoot * numOfLevels + 1];
    public Terrain terrain;

    private bool isCalibrated;

    //when curRound ==15,change display text into"Take a break!"
    private DisplayTrial displayTrial;


    void Start()
    {
        isCalibrated = false;
        headset = GameObject.Find("[CameraRig]");

        // terrain = this.GetComponent<Terrain>();
        terrain.terrainData.size = new Vector3(2, 0.1f, 2);
        Shuffle();

        userEndtask.AddOnStateDownListener(TriggerDown, handType);
        //userEndtrial.AddOnStateDownListener(TriggerDownDouble, handType);
        userAdjustup.AddOnStateDownListener(NorthDown, handType);
        userAdjustdown.AddOnStateDownListener(SouthDown, handType);

        if(whichFirst == "right")
        {
            LR = 'R';
        }
        else
        {
            LR = 'L';
        }
        curRound = 0;
        //when curRound ==15,change display text into"Take a break!"
        displayTrial = GameObject.Find("Controller (right)").GetComponentInChildren<DisplayTrial>();
    }

    private void TriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        Debug.Log("Trigger is down, trial no. " + (curRound + 1));
        //when curRound ==15,change display text into"Take a break!"

        if (curRound < order.Count && curRound != (trialsPerFoot * numOfLevels))
        {
            // save result off last round
            dataCollected[curRound] = terrain.terrainData.size.y;
            // notify user for round change
            Pulse(1, 150, 75, handType);

            
            // next round
            curRound++;
            //地形reset成最光滑
            terrain.terrainData.size = new Vector3(2, 0, 2);

            // check if it is time for swap foot or study finished
            if (curRound < order.Count && curRound != (trialsPerFoot * numOfLevels))
            {
                SendSerialMessage(LR, "C");
                SendSerialMessage(LR, "L" + order[curRound]);
                /*oscParser.SendOSCMessage(LR + " C");
                oscParser.SendOSCMessage(LR + " L" + order[curRound]);*/
            }
        }
        else if (curRound == 15)
        {
            curRound = 15;
        }
        if (curRound == order.Count)
        {
            WriteCsv();
            SendSerialMessage('B', "C");
            SendSerialMessage('B', "L5");
        }

        //timer
        //displayTrial.text.setTextColor(R.color.Red);
        //換trial 紅色字提醒user不能動
        displayTrial.text.color = Color.red;
        Invoke("ChangeColor", 3.0f);

    }

    // private void TriggerDownDouble(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    // {
    //     // FIXME: double down and gg?
    //     Debug.Log("Trigger is double down");
    //     WriteCsv();
    //     SendSerialMessage('B', "C");
    //     SendSerialMessage('B', "L5");
    //     /*oscParser.SendOSCMessage("L C");
    //     oscParser.SendOSCMessage("L L4");
    //     oscParser.SendOSCMessage("R C");
    //     oscParser.SendOSCMessage("R L4");*/
    // }


    private void NorthDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {

        float currHeight = terrain.terrainData.size.y;
        if (currHeight + modifyheight < 15.6f)
        {
            Debug.Log("north is down");
            terrain.terrainData.size = new Vector3(2, currHeight + modifyheight, 2);
            headset.transform.position = new Vector3(headset.transform.position.x, headset.transform.position.y + modifyheight * 0.01f, headset.transform.position.z);
            print(currHeight);
        }
        else
        {
            print("out of input range");
            Pulse(0.5f, 150, 75, handType);
        }

    }

    private void SouthDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {

        float currHeight = terrain.terrainData.size.y;
        // FIXME: >= 0.1?
        if (currHeight - modifyheight > 0.1f)
        {
            Debug.Log("south is down");
            terrain.terrainData.size = new Vector3(2, currHeight - modifyheight, 2);
            headset.transform.position = new Vector3(headset.transform.position.x, headset.transform.position.y - modifyheight * 0.01f, headset.transform.position.z);
            print(currHeight);
        }
        else
        {
            print("out of input range");
            Pulse(0.5f, 150, 75, handType);
        }
    }
    private void Pulse(float duration, float frequency, float amplitude, SteamVR_Input_Sources source)
    {
        // create vibration
        hapticAction.Execute(0, duration, frequency, amplitude, source);
    }

    void Update()
    {
        //更新display trial文字
        if (curRound == 31)
        {
            displayTrial.text.text = "Done!";
        }
        else if (curRound < 15)
        {
            displayTrial.text.text = "Trial " + (curRound+1);
        }
        else if (curRound == 15)
        {
            displayTrial.text.text = "Take a break!";
        }
        else
        {
            displayTrial.text.text = "Trial " + curRound;

        }

        // Calibration
        if (Input.GetKeyDown(KeyCode.N))
        {
            SendSerialMessage('B', "N", false);
            isCalibrated = true;
        }

        // Emergancy: Clear Target
        if (Input.GetKeyDown(KeyCode.C))
        {
            SendSerialMessage('B', "C", false);
        }

        // Initialize
        if (Input.GetKeyDown(KeyCode.I))
        {
            curRound = 0;
            SendSerialMessage('B', "C");
            if (LR == 'R')
            {
                SendSerialMessage('L', "L5");
                SendSerialMessage('R', "L" + order[curRound]);
            }
            else
            {
                SendSerialMessage('R', "L5");
                SendSerialMessage('L', "L" + order[curRound]);
            }
        }

        // press space to start next foot
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(curRound == trialsPerFoot * numOfLevels)
            {
                Debug.Log("next foot");
                dataCollected[curRound] = 0;

                // lock previous shoe
                SendSerialMessage(LR, "L5");

                if (LR == 'R')  LR = 'L';
                else LR = 'R';

                // switch to next level
                curRound++;

                SendSerialMessage(LR, "L" + order[curRound]);
                
            }
        }

        // print motor position
        if (Input.GetKeyDown(KeyCode.P))
        {
            leftShoe.PrintMotorPos();
            rightShoe.PrintMotorPos();
        }

        // FIXME: not sure what this for
        if (Input.GetKeyDown(KeyCode.A))
        {
            // set to maximum height?
            terrain.terrainData.size = new Vector3(2, 20, 2);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log(dataCollected.Length);
            Debug.Log(order.Count);
        }

        // set to level 1
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SendSerialMessage(LR, "L1");
        }
        // set to level 2
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SendSerialMessage(LR, "L2");
        }
        // set to level 3
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SendSerialMessage(LR, "L3");
        }
        // set to level 4
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SendSerialMessage(LR, "L4");
        }
        // set to level 5
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            SendSerialMessage(LR, "L5");
        }

        // test next round
        if (Input.GetKeyDown(KeyCode.L))
        {
            if (curRound < order.Count && curRound != trialsPerFoot * numOfLevels)
            {
                dataCollected[curRound] = terrain.terrainData.size.y;
                curRound++;
                // check if it is time for swap foot or study finished
                if (curRound < order.Count && curRound != trialsPerFoot * numOfLevels)
                {
                    SendSerialMessage(LR, "C");
                    SendSerialMessage(LR, "L" + order[curRound]);
                }
            }
        }

        // test previous round
        if (Input.GetKeyDown(KeyCode.K))
        {
            // TODO: test this
            if (curRound > 0 && curRound != trialsPerFoot * numOfLevels + 1)
            {
                curRound--;
                SendSerialMessage(LR, "C");
                SendSerialMessage(LR, "L" + order[curRound]);
            }
            else if (curRound != trialsPerFoot * numOfLevels + 1)
            {
                SendSerialMessage(LR, "C");
                SendSerialMessage(LR, "L5");

                if (LR == 'R') LR = 'L';
                else LR = 'R';

                curRound += 2;
                SendSerialMessage(LR, "C");
                SendSerialMessage(LR, "L" + order[curRound]);
            }
        }

        // test save file
        if (Input.GetKeyDown(KeyCode.S))
        {
            WriteCsv();
        }


    }

    void SendSerialMessage(char whichShoe, string message, bool calibratedCheck = true)
    {
        // calibration check
        if (calibratedCheck && !isCalibrated)
        {
            Debug.LogWarning("The device is not calibrated yet");
            return;
        }

        if (whichShoe == 'R')
        {
            // right
            rightShoe.SendSerialMessage(message);
        }
        else if (whichShoe == 'L')
        {
            // left
            leftShoe.SendSerialMessage(message);
        }
        else if (whichShoe == 'B')
        {
            // both
            rightShoe.SendSerialMessage(message);
            leftShoe.SendSerialMessage(message);
        }
        else
        {
            Debug.LogError("unknown shoe: " + whichShoe);
        }
    }

    void Shuffle()
    {
        // levels
        List<int> levels = new List<int>();
        for (int i = 0; i < numOfLevels; ++i)
        {
            levels.Add(i + 1);
        }

        for (int i = 0; i < 2 * trialsPerFoot; i++)
        {
            // for each trial, shuffle the levels
            for (int j = levels.Count - 1; j > 0; j--)
            {
                // Randomize a number between 0 and i (so that the range decreases each time)
                int rand = UnityEngine.Random.Range(0, j);

                // Save the value of the current i, otherwise it'll overwrite when we swap the values
                int temp = levels[j];

                // Swap the new and old values
                levels[j] = levels[rand];
                levels[rand] = temp;
            }
            order.AddRange(levels);

            // change foot
            if (i == trialsPerFoot - 1)
            {
                order.Add('-');
            }
            //print(order.Count);   
        }

        // display in console
        int k = 0;
        string orderStr = "";
        for (int i = 0; i < 2 * trialsPerFoot; ++i)
        {
            orderStr += "[";
            for (int j = 0; j < levels.Count; ++j)
            {
                orderStr += order[k] + ", ";
                k++;
            }
            orderStr += "]";
            if (i == trialsPerFoot - 1)
            {
                orderStr += "\n";
                k++;
            }
        }
        Debug.Log(orderStr);

    }

    void WriteCsv()
    {
        int fileUserID = 0;
        string filePath = "C://Users/hsnuh/OneDrive/Desktop/FrictShoes/perception study/results/user" + userID + ".csv";
        // TODO: text this
        while (File.Exists(filePath))
        {
            fileUserID++;
            filePath = "C://Users/hsnuh/OneDrive/Desktop/FrictShoes/perception study/results/user" + userID + "_" + fileUserID + ".csv";
        }
        StreamWriter writer = new StreamWriter(filePath);

        writer.WriteLine("userID," + userID);
        writer.WriteLine("whichFirst," + whichFirst);

        for (int i = 0; i < dataCollected.Length; i++)
        {
            writer.WriteLine(order[i] + "," + dataCollected[i]);
        }
        writer.Close();

        Debug.Log("File saved");
    }
    private void OnApplicationQuit()
    {
        // TODO: test this
        // In any case
        WriteCsv();
        SendSerialMessage('B', "C");
        SendSerialMessage('B', "L5");
    }

    void ChangeColor()
    {
        displayTrial.text.color = new Color(255, 255, 255, 255);
    }

}
