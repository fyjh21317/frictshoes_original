﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using extOSC;

public class OSCParser : MonoBehaviour
{
    public string Address = "/sendToUnity";

    // [Header("OSC Settings")]
    public OSCReceiver receiver;
    public OSCTransmitter transmitter;
    public bool printLog = false;

    void Start()
    {
        //receiver = GetComponent<OSCReceiver>();
        //transmitter = GetComponent<OSCTransmitter>();

        receiver.Bind(Address, ReceiveOSCMessage);
    }

    void ReceiveOSCMessage(OSCMessage message){
        if(printLog){
            Debug.LogFormat("Received: {0}", message);
        }
    }

    public void SendOSCMessage(string message){
        OSCMessage oscMessage = new OSCMessage(Address);
        oscMessage.AddValue(OSCValue.String(message));
        Debug.LogFormat("Transmit: {0}", message);

        transmitter.Send(oscMessage);
    }
}
