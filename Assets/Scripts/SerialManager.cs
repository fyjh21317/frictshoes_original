﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System.IO.Ports;

public class SerialManager : MonoBehaviour
{
    public string port;
    public int baudRate = 115200;
    public char LR;
    private SerialPort serialPort;

    private int[] motorPos = new int[6];
    private int[] targetPos = new int[6];

    private bool isReady;
    //0825 Merge
    private bool isInPosition;

    void Start()
    {
        isReady = false;
        //0825 Merge
        isInPosition = false;
        // initialize serial port
        serialPort = new SerialPort(port, baudRate);
        serialPort.ReadTimeout = 1;
        serialPort.Open();

    }

    void Update()
    {
        // try to get message
        try
        {
            if (serialPort.IsOpen)
            {
                string message = serialPort.ReadLine();
                Debug.Log(LR + " Received: " + message);

                if (message.Substring(2).Equals("Ready"))
                {
                    if(message[0] == LR)
                    {
                        isReady = true;
                    }
                    else
                    {
                        Debug.LogError("left right shoes are wrong");
                    }
                    
                }

                // record motor position
                if (message.StartsWith("M"))
                {
                    message = message.Trim(" "[0]);
                    string[] arr = message.Split(","[0]);
                    foreach(string msg in arr)
                    {
                        int motor = int.Parse(msg.Substring(1, 1));
                        int pos = int.Parse(msg.Substring(3));
                        motorPos[motor] = pos;
                    }
                    isInPosition = false;
                }

                if(message.Equals("Target reached"))
                {
                    isInPosition = true;
                }
            }
        }
        catch
        {

        }

        

    }

    public void SendSerialMessage(string message)
    {
        if (serialPort.IsOpen)
        {
            serialPort.WriteLine(message);
            serialPort.BaseStream.Flush();
            Debug.Log(LR + " Transmit: " + message);
        }
        else
        {
            Debug.LogError("Fail to send message to Arduino: " + message);
        }
    }

    public void PrintMotorPos()
    {
        string message = LR + " motor pos:\t";
        foreach (int pos in motorPos)
        {
            message += pos + ",\t";
        }
        message += "\n" + LR + " target pos:\t";
        foreach (int pos in targetPos)
        {
            message += pos + ",\t";
        }
        Debug.Log(message);
    }

    void CloseDevice()
    {
        SendSerialMessage("L5");
        //serialPort.Close();
    }

    private void OnDisable()
    {
        CloseDevice();
    }

    private void OnApplicationQuit()
    {
        CloseDevice();
    }

}

