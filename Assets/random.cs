﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System;
using Valve.VR;

public class random : MonoBehaviour
{
    
    //input user number beforce begin
    public string userNo;
    public string whichFirst;
    public float modifyheight;

    // a reference to the action
    public SteamVR_Action_Boolean userEndtrial;
    public SteamVR_Action_Boolean userEndtask;
    public SteamVR_Action_Boolean userAdjustup;
    public SteamVR_Action_Boolean userAdjustdown;
    
    // a reference to the hand
    public SteamVR_Input_Sources handType;
    public SteamVR_Action_Vibration hapticAction;

    // current feet (L/R)
    private char LR;
    private int curRound;
    private const int trialsPerFoot = 4;
    private const int numOfLevels = 4;

    private GameObject headset;
    private OSCParser oscParser;
    private List<int> order = new List<int>();
    //private List<float> dataCollected = new List<float>();
    private float[] dataCollected = new float[2*trialsPerFoot*numOfLevels+1];
    private Terrain terrain;

    void Start()
    {
        headset = GameObject.Find("[CameraRig]");
        oscParser = GameObject.Find("OSC Manager").GetComponent<OSCParser>();
        
        terrain = this.GetComponent<Terrain>();
        terrain.terrainData.size = new Vector3(2,0.1f,2);
        shuffle();
        //print(resolHeight);

        userEndtask.AddOnStateDownListener(TriggerDown, handType);
        userEndtrial.AddOnStateDownListener(TriggerDownDouble, handType);
        userAdjustup.AddOnStateDownListener(NorthDown, handType);
        userAdjustdown.AddOnStateDownListener(SouthDown, handType);
        //userAdjust.AddOnStateUpListener(TriggerUp, handType);

        // set left/right
        if(whichFirst == "right"){
            LR = 'R';
        }else{
            LR = 'L';
        }
        curRound = 0;

        // set current as level 4
        // oscParser.SendOSCMessage("L S");
        // oscParser.SendOSCMessage("R S");
        // // start first level
        // oscParser.SendOSCMessage(LR + " C");
        // oscParser.SendOSCMessage(LR + " L" + order[curRound]);


        print(terrain.terrainData.size.y);
    }

    private void TriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource){
        Debug.Log("Trigger is down, trial no. "+(curRound+1));

        if(curRound < order.Count && curRound != trialsPerFoot*numOfLevels){
            dataCollected[curRound] = terrain.terrainData.size.y;
            Pulse(1,150,75,handType);
            curRound++;
            if(curRound < order.Count && curRound != trialsPerFoot*numOfLevels){
                oscParser.SendOSCMessage(LR + " C");
                oscParser.SendOSCMessage(LR + " L" + order[curRound]);
            }
        }

    }

    private void TriggerDownDouble(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource){
        Debug.Log("Trigger is double down");
        writeCsv();
        oscParser.SendOSCMessage("L C");
        oscParser.SendOSCMessage("L L4");
        oscParser.SendOSCMessage("R C");
        oscParser.SendOSCMessage("R L4");
}

    private void NorthDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource){
        
        float currHeight = terrain.terrainData.size.y;
        if (currHeight+modifyheight < 15.6f){
            Debug.Log("north is down");
            terrain.terrainData.size = new Vector3(2,currHeight+modifyheight,2);
            headset.transform.position = new Vector3(headset.transform.position.x, headset.transform.position.y+modifyheight*0.01f,headset.transform.position.z);
            print(currHeight);
        }
        else{
            print("out of input range");
            Pulse(0.5f,150,75,handType);
        }
        
    }

    private void Pulse(float duration, float frequency, float amplitude, SteamVR_Input_Sources source){
        hapticAction.Execute(0, duration, frequency, amplitude, source);
    }

    private void SouthDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource){
        
        float currHeight = terrain.terrainData.size.y;
        if (currHeight-modifyheight > 0.1f){
            Debug.Log("south is down");
            terrain.terrainData.size = new Vector3(2,currHeight-modifyheight,2);
            headset.transform.position = new Vector3(headset.transform.position.x, headset.transform.position.y-modifyheight*0.01f,headset.transform.position.z);
            print(currHeight);
        }
        else{
            print("out of input range");
            Pulse(0.5f,150,75,handType);            
        }
    }


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)){
            if(curRound == trialsPerFoot*numOfLevels){
                print("next foot");
                dataCollected[curRound] = 0;
                
                // locked previous shoe
                oscParser.SendOSCMessage(LR + " L4");

                if(LR == 'R') LR = 'L';
                else LR = 'R';

                // switch to next level
                curRound++;
                oscParser.SendOSCMessage(LR + " L" + order[curRound]);

            }
        }


        if (Input.GetKeyDown(KeyCode.A))
        {
            // set to maximum height?
            terrain.terrainData.size = new Vector3(2, 20, 2);
            //dataCollected[curRound] = terrain.terrainData.size.y;
            //curRound++;
        }
       

        if (Input.GetKeyDown(KeyCode.E)) //controller input
        {
            print(dataCollected.Length);
            print(order.Count);
            //writeCsv();
        }

        if(Input.GetKeyDown(KeyCode.N)){
            // test next level
            if(curRound < order.Count && curRound != trialsPerFoot*numOfLevels){
                dataCollected[curRound] = terrain.terrainData.size.y;
                curRound++;
                if(curRound < order.Count && curRound != trialsPerFoot*numOfLevels){
                    oscParser.SendOSCMessage(LR + " C");
                    oscParser.SendOSCMessage(LR + " L" + order[curRound]);
                }
            }
        }

        if(Input.GetKeyDown(KeyCode.P)){
            // test previous level
            // FIXME: should be fixed
            curRound--;
            if(order[curRound] != 'n'){
                oscParser.SendOSCMessage(LR + " C");
                oscParser.SendOSCMessage(LR + " L" + order[curRound]);
            }
        }

        if(Input.GetKeyDown(KeyCode.S)){
            // test save file
            writeCsv();
        }

        if(Input.GetKeyDown(KeyCode.Alpha1)){
            // set to level 1
            oscParser.SendOSCMessage("L L1");
            oscParser.SendOSCMessage("R L1");
        }
        if(Input.GetKeyDown(KeyCode.Alpha2)){
            // set to level 2
            oscParser.SendOSCMessage("L L2");
            oscParser.SendOSCMessage("R L2");
        }
        if(Input.GetKeyDown(KeyCode.Alpha3)){
            // set to level 3
            oscParser.SendOSCMessage("L L3");
            oscParser.SendOSCMessage("R L3");
        }
        if(Input.GetKeyDown(KeyCode.Alpha4)){
            // set to level 4
            oscParser.SendOSCMessage("L L4");
            oscParser.SendOSCMessage("R L4");
        }
        if(Input.GetKeyDown(KeyCode.I)){
            // set current as level 4
            oscParser.SendOSCMessage("L S");
            oscParser.SendOSCMessage("R S");
            // start first level
            oscParser.SendOSCMessage(LR + " C");
            oscParser.SendOSCMessage(LR + " L" + order[curRound]);
        }


    }

    void shuffle()
    {
        // levels
        List<int> levels = new List<int>();
        for(int i = 0; i < numOfLevels; ++i){
            levels.Add(i+1);
        }

        for (int i = 0; i < 2 * trialsPerFoot; i++)
        {
            // for each trial, shuffle the levels
            for (int j = levels.Count - 1; j > 0; j--)
            {
                // Randomize a number between 0 and i (so that the range decreases each time)
                int rand = UnityEngine.Random.Range(0, j);

                // Save the value of the current i, otherwise it'll overwrite when we swap the values
                int temp = levels[j];

                // Swap the new and old values
                levels[j] = levels[rand];
                levels[rand] = temp;
            }
            order.AddRange(levels);

            // change foot
            if (i == trialsPerFoot-1){
                order.Add('n');
            }
            //print(order.Count);   
        }

        // display in console
        int k = 0;
        string orderStr = "";
        for(int i = 0; i < 2 * trialsPerFoot; ++i){
            orderStr += "[";
            for(int j = 0; j < levels.Count; ++j){
                orderStr += order[k] + ", ";
                k++;
            }
            orderStr += "]";
            if (i == trialsPerFoot-1){
                orderStr += "\n";
                k++;
            }
        }
        Debug.Log(orderStr);
        
    }

    void writeCsv()
    {
        string filePath = "C://Users/RobinLab/frictShoes_force study/user" + userNo + ".csv";
        StreamWriter writer = new StreamWriter(filePath);

        writer.WriteLine("userNo," + userNo);
        writer.WriteLine("whichFirst," + whichFirst);

        for(int i = 0; i < dataCollected.Length; i++)
        {
            writer.WriteLine(order[i] +","+ dataCollected[i]);
        }
        writer.Close();
    }


    
}
