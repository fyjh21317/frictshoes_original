﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainModify : MonoBehaviour {
	public Terrain terrain;
	private Texture2D brush;
	
	private TerrainData terrainData;
	private Vector3 terrainSize;
	private int heightmapHeight;
 	private int heightmapWidth;
 	private float[,] heightmapData;


    // Use this for initialization
    [System.Obsolete]
    void Start () {

		GetTerrainData();
		ResetHeights();
		//paintBrush(brush);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [System.Obsolete]
    void GetTerrainData() {
		if ( !terrain )
			terrain = Terrain.activeTerrain;
	
		terrainData = terrain.terrainData;
	
		terrainSize = terrain.terrainData.size;
	
		heightmapWidth = terrain.terrainData.heightmapWidth;
		heightmapHeight = terrain.terrainData.heightmapHeight;
	
		heightmapData = terrainData.GetHeights( 0, 0, heightmapWidth, heightmapHeight );
    }

	void ResetHeights() // FOR TESTING, reset to flat terrain
	{
		int x;
		int z;
		
		for ( z = 0; z < heightmapHeight; z ++ )
		{
			for ( x = 0; x < heightmapWidth; x ++ )
			{
				heightmapData[ z, x ] = 0;
			}
		}
		
		terrainData.SetHeights( 0, 0, heightmapData );
	} 
	void paintBrush(Texture2D brushTexure) //input brush?
	{
		int x;
		int z;
		// int heightX;
		// int heightY;
		// int heightZ;
		Vector2 calc;
		
		for ( z = -brushTexure.height; z <= brushTexure.height; z ++ )
		{
			for ( x = -brushTexure.width; x <= brushTexure.width; x ++ )
			{
				// for a circle, calcualate a relative Vector2
				calc = new Vector2( x, z );
				// check if the magnitude is within the circle radius
				
			}
		}
		
		// apply new heights to terrainData
		terrainData.SetHeights( 0, 0, heightmapData );
	}
}
